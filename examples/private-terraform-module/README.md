# Private Terraform module

This example shows how to run Infracost on GitLab CI with a Terraform project that uses a private Terraform module. This requires a variable to be added to your GitLab repository called `GIT_SSH_KEY` containing a private key so that Infracost can access the private repository.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: private-terraform-module)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/private-terraform-module/code

stages:
  - infracost

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  script:
    # Add the git SSH key
    - mkdir -p .ssh
    - echo "$GIT_SSH_KEY" > .ssh/git_ssh_key
    - chmod 400 .ssh/git_ssh_key
    - export GIT_SSH_COMMAND="ssh -i $(pwd)/.ssh/git_ssh_key -o 'StrictHostKeyChecking=no'"
    # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
    - git clone $CI_REPOSITORY_URL --branch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --single-branch /tmp/base

    # Generate an Infracost cost snapshot from the comparison branch, so that Infracost can compare the cost difference.
    - |
      infracost breakdown --path=/tmp/base/${TF_ROOT} \
                          --format=json \
                          --out-file=infracost-base.json

    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT} \
                     --compare-to=infracost-base.json \
                     --format=json \
                     --out-file=infracost.json

    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
