# Sentinel Example

This example shows how to write [cost policies](https://www.infracost.io/docs/features/cost_policies/) with HashiCorp's [Sentinel](https://www.hashicorp.com/sentinel). For simplicity, this example evaluates policies with the Sentinel CLI (a.k.a. Sentinel Simulator). The point of this example is to show how a policy could be written against the Infracost JSON format, not how to run Sentinel, since that's tied to HashiCorp's cloud platform.

When the policy checks passes it outputs `Policy check passed.` in the logs. When the policy checks fail, the logs show the Sentinel output indicating failing policies.

Create a policy file (e.g. `policy.policy`) that checks a global parameter 'breakdown' containing the Infracost JSON:

```policy
import "strings"

limitTotalDiff = rule {
  float(breakdown.totalMonthlyCost) < 1500
}

awsInstances = filter breakdown.projects[0].breakdown.resources as _, resource {
  strings.split(resource.name, ".")[0] is "aws_instance"
}

limitInstanceCost = rule {
  all awsInstances as _, instance {
    float(instance.hourlyCost) <= 2.00
  }
}

instanceBaseCost = func(instance) {
  cost = 0.0
 	for instance.costComponents as cc {
    cost += float(cc.hourlyCost)
  }
  return cost
}

instanceIOPSCost = func(instance) {
  cost = 0.0
 	for instance.subresources as sr {
    for sr.costComponents as cc {
      if cc.name == "Provisioned IOPS" {
        cost += float(cc.hourlyCost)
      }
    }
  }
  return cost
}

limitInstanceIOPSCost = rule {
  all awsInstances as _, instance {
    instanceIOPSCost(instance) <= instanceBaseCost(instance)
  }
}

main = rule {
  limitTotalDiff and
  limitInstanceCost and
  limitInstanceIOPSCost
}
```

Then use Sentinel to test infrastructure cost changes against the policy by adding the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: sentinel)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/terraform-project/code

stages:
  - infracost
  - policy_check

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  script:
    # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
    - git clone $CI_REPOSITORY_URL --branch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --single-branch /tmp/base

    # Generate an Infracost cost snapshot from the comparison branch, so that Infracost can compare the cost difference.
    - |
      infracost breakdown --path=/tmp/base/${TF_ROOT} \
                          --format=json \
                          --out-file=infracost-base.json

    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT} \
                     --compare-to=infracost-base.json \
                     --format=json \
                     --out-file=infracost.json
  artifacts:
    paths:
      - infracost.json
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

sentinel:
  stage: policy_check
  image:
    name: hashicorp/sentinel:latest
    entrypoint: [""]
  script:
    - sentinel apply -color=false -global breakdown="$(cat infracost.json)" examples/sentinel/policy/policy.policy
    - echo "Policy check passed."
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
